## IoT Basics
***

### Industrial Revolution 
***
* **Industry 1.0 (1784):**  
  * Mechanisation, Steam power & Weaving loom.
* **Industry 2.0 (1870):**  
  * Mass Production, Assembly line & Electrical energy.
* **Industry 3.0 (1969):**  
  * Automation, Computers & Electronics.
  * Data are stored in databases and represented in excels.
* **Industry 4.0 (Today):**  
  * Heavy usage of _Internet_.
  * Cyber Physical Systems, Internet of Things and Networking 

### Industry 3.0 v/s Industry 4.0
***
#### Industry 3.0
***
* **Industry 3.0 use Sensors, PLC's, SCADA and ERP.**
* **Workflow:** _Sensors_ --> _PLC_ --> _SCADA & ERP_
* **Architecture of Industry 3.0:**  
	![Architecture](/assignments/summary/images/image1.png)

* **Communication Protocols:**  
  * The _Sensors_ installed in the factory send data to PLCs by means of _**Field Bus**_, which are basically certain Protocols such as Modbus, CANopen, EtherCAT, etc.

#### Industry 4.0
***

* **Industry 4.0 is _Industry 3.0_ connected to Internet, which is called IoT.**  

* Connecting to Internet makes data communication faster, easier without data loss.
* Data Gathering brings more advancements like : 
  * Showing Dashboards
  * Remote Web SCADA
  * Remote control configuration of devices.
  * Predictive maintenance.
  * Real-time event processing.
  * Analytics with predictive models.
  * Automated device provisioning (Auto discovery).
  * Real-time alerts & alarms.  etc..  
  
* Here _Data_ are stored in **Cloud**, which decreases human efforts in data management.  
* **Communication Protocols:**  
  * Some _Protocols_ used to send data to _cloud_ for data analysis are : MQTT.org, AMQP, OPC UA, CoAP RFC 7252, HTTP, WebSocket, RESTful API etc.  

* **Architecture of Industry 4.0:**  
  
    ![Architecture](/assignments/summary/images/image2.png)

#### Problems faced by Factory Owners for Industry 4.0 Upgradation and their solution
***
#### Problems

1. **Cost:** Factory owners don't want to switch into Industry 4.0, because it is _Expensive_.
2. **Downtime:** Changing Hardware would result in downtime and nobody want to face such loss.
3. **Reliability:** Investing in devices which are unproven and unreliable is a risk.

#### Solutions

* Its pretty solution is : "Getting data from the devices already present in the factory and sending it to cloud."
* So, there is no need to replace the original devices, causing lower risk factor.

* In simple language, "Get data from _Industry 3.0_ and convert it to _Industry 4.0_."
* We have to **Change the _Industry 3.0 Protocols_ to _Industry 4.0 Protocols_.**

* However some Challenges faced in this Conversion are :
  * Expensive Hardware
  * Lack of documentation
  * Propritary PLC Protocols

#### How to Convert
***

* We have a library that helps get data from _Industry 3.0 devices_ and send to _Industry 4.0 cloud_.

	![Diagram of Conversion of 3.0 into 4.0](/assignments/summary/images/image3.png)

#### Steps to make an IIoT Product
***
1. Identify most popular Industry 3.0 devices.
2. Study Protocols that these devices Communicate.
3. Get data from the Industry 3.0 devices.
4. Send the data to cloud for Industry 4.0.

#### What's Next
***
* After sending data to cloud, Data Analysis is carried out with the help of several tools.
* Various tools are: 
  * **IoT TSDB Tools:**  
    * Store the data in Time series databases.
    * Eg: Prometheus, InfluxDB.
  * **IoT Dashboards:**  
    * View all the data into beautiful dashboards.
    * Eg: Grafana, Thingsboard.
  * **IoT Platforms:**  
    * Analyse the data on the platforms like; AWS IoT, Google IoT, Azure IoT, Thingsboard.
  * **Getting Alerts**  
    * Get Alerts based on your data using the platforms like; Zaiper, Twilio.
